package SnakeGame;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Entity {
private int x,y,size;

public Entity(int size) {
	this.size = size;
}

public int getX() {return x;}
public int getY() {return y;}
public void setX(int x) {this.x = x;}
public void setY(int y) {this.y = y;}

public void setPosition(int x, int y) {
	this.x = x;
	this.y = y;
}

public void move(int dx,int dy) {
	x+=dx;
	y+=dy;
}

public Rectangle getBound() {
	return new Rectangle(x,y,size,size);
}
public boolean isColision(Entity o) {
	if(o == this) return false;
	return getBound().intersects(o.getBound());
}

public void render(Graphics2D g2d) {
	g2d.draw(new Ellipse2D.Double(x - 1, y - 1, size -2, size-2));
	//g2d.fillRect(x + 1, y + 1, size - 2, size - 2);
}

public void renderSuperPoint(Graphics2D g2d) {
	g2d.fill(new Ellipse2D.Double(x-1, y-1, size, size));
}

public void headRender(Graphics2D g2d) {
    g2d.fill(new Ellipse2D.Double(x-1, y-1, size, size));

}
}

