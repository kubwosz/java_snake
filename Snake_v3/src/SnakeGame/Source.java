package SnakeGame;

public class Source {
	public boolean pause;
	public boolean start;
	public int speed;
	
	public Source() {
		pause = false;
		start = false;
		speed = 1;
	}
	// flag = true - set, flag = flase - get
	public synchronized boolean PauseMethod(boolean arg, boolean flag ) {
		if(flag == true)
			pause = arg;
		
		return pause;
	}
	
	// flag = true - set, flag = flase - get
	public synchronized boolean StartMethod(boolean arg, boolean flag ) {
		if(flag == true)
			start = arg;
		
		return start;
	}
	
	// flag = true - set, flag = flase - get
	public synchronized int SpeedMethod(int v, boolean flag) {
		if(flag == true)
			speed = v;
		return speed;
	}
}
