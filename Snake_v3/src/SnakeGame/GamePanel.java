package SnakeGame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.Console;
import java.util.ArrayList;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable, KeyListener {

	public static final int WIDTH =400;
	public static final int HEIGHT =200;
	//Redner
	private Graphics2D g2d;
	private BufferedImage image;
	//Game Loop
	private Thread thread;
	private boolean running;
	private long targetTime;
	
	//Game stuff
	private final int SIZE = 10;
	private Entity head;
	private ArrayList<Entity> snake, points, superPoints;
	private int score;
	private int level;
	private boolean gameover;
	private Source source;
	//movement
	private int dx,dy;
	//key input
	private boolean up,down,right,left,start;
	public boolean pause;
	public GamePanel(Source s) {
		source = s;
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
	}

	public void setFPS(int fps)
	{
		targetTime=1000/fps;
	}
	@Override
	public void keyPressed(KeyEvent e) {
		int k = e.getKeyCode();

		if(k== KeyEvent.VK_UP) up = true;
		if(k== KeyEvent.VK_DOWN) down = true;
		if(k== KeyEvent.VK_LEFT) left = true;
		if(k== KeyEvent.VK_RIGHT) right = true;
		if(k== KeyEvent.VK_ENTER) start = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int k = e.getKeyCode();
		
		if(k== KeyEvent.VK_UP) up = false;
		if(k== KeyEvent.VK_DOWN) down = false;
		if(k== KeyEvent.VK_LEFT) left = false;
		if(k== KeyEvent.VK_RIGHT) right = false;
		if(k== KeyEvent.VK_ENTER) start = false;
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void run() {
		if(running)
			return;
		init();
		long startTime;
		long elapsed;
		long wait;
		while(running){

			startTime = System.nanoTime();
			setFocusable(true);
			requestFocus();
			if(!source.PauseMethod(pause, false)) {
				update();
			}
			requestRender();
			
			elapsed = System.nanoTime() - startTime;
			wait = targetTime - elapsed / 1000000;
			if(wait > 0){
				try{
					Thread.sleep(wait);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
					
		}	
	}
	
	
	public void init(){
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
		g2d = image.createGraphics();
		running = true;
		setUpLevel();
		gameover = false;
		level = 1;
		setFPS(15);
	}
	
	private void setUpLevel() {
		snake = new ArrayList<Entity>();
		points = new ArrayList<Entity>();
		superPoints = new ArrayList<Entity>();
		
		head = new Entity(SIZE);
		head.setPosition(WIDTH/2,HEIGHT/2);
		snake.add(head);
		
		for(int i=1;i<3;i++) {
			Entity e = new Entity(SIZE);
			e.setPosition(head.getX() + (i*SIZE), head.getY());
			snake.add(e);
		}
		Entity p1 = new Entity(SIZE);
		Entity p2 = new Entity(SIZE);
		Entity p3 = new Entity(SIZE);
		setPoint(p1);
		setPoint(p2);
		setPoint(p3);
		points.add(p1);
		points.add(p2);
		superPoints.add(p3);
		
		score = 0;
		gameover = false;
		dx = dy = 0;
	}
	public void setPoint(Entity p) {
		int x = (int)(Math.random()*(WIDTH -  SIZE));
		int y = (int)(Math.random()*(HEIGHT-  SIZE));
		x = x - (x%SIZE);
		y = y - (y%SIZE);
		if(x < 0) x = SIZE;
		if(y < 0) y = SIZE;
		if(y > 170) y = 160; // bug
		p.setPosition(x,y);
	}
	
	private void requestRender() {
		// TODO Auto-generated method stub
		render(g2d);
		Graphics g = getGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();
	}
	
	private void update() {
		boolean addflag = false;
		if(gameover) {
			start = source.StartMethod(start, false);
			if(start) {
				level = 1;
				start = source.StartMethod(false,true);
				setUpLevel();
				gameover = false;
			}
			return;
		}
		
	if(up && dy == 0) {
		dy = -SIZE;
		dx=0;
	}
	if(down && dy == 0) {
		dy = SIZE;
		dx=0;
	}
	if(left && dx == 0) {
		dy = 0;
		dx=-SIZE;
	}
	if(right && dx == 0 && dy != 0) {
		dy = 0;
		dx=SIZE;
	}
	if((dx != 0 || dy !=0) /*&& source.StartMethod(start, false)*/) {
	for(int i = snake.size() -1;i > 0;i--) {
		snake.get(i).setPosition(
				snake.get(i-1).getX(),
				snake.get(i-1).getY()
				);
	}
	head.move(dx,dy);
	}
	
	for(Entity e :  snake) {
		if(e.isColision(head)){
			gameover = true;
			break;
		}
	}
	
	for(Entity e : points) {
		if(e.isColision(head)) {
			score++;
			setPoint(e);
			
			Entity es = new Entity(SIZE);
			es.setPosition(-100,-100);
			snake.add(es);
			
			if(score%10 == 0) {
				addflag = true;
				level++;
				if(level>10)level=10;
				setFPS(level * 10);
			}
		}
	}
	
	for(Entity e : superPoints) {
		if(e.isColision(head)) {
			score++;
			setPoint(e);
			
			for(int i = 0; i < 4 ; ++i) {
				Entity e1 = new Entity(SIZE);
				e1.setPosition(-100,-100);
				snake.add(e1);
			}
			
			if(score%10 == 0) {
				addflag = true;
				level++;
				if(level>10)level=10;
				setFPS(level * 10);
			}
		}
	}
	
	if(addflag) {
		Entity p1 = new Entity(SIZE);
		Entity p2 = new Entity(SIZE);
		setPoint(p1);
		setPoint(p2);
		points.add(p1);
		superPoints.add(p2);
		addflag=false;
	}
	
	
	setFPS(source.SpeedMethod(level, false) / 5 + 5);
	
	if(head.getX()<0) head.setX(WIDTH - SIZE);
	if(head.getY()<0) head.setY(HEIGHT - SIZE);
	if(head.getX()>WIDTH - SIZE) head.setX(0);
	if(head.getY()>HEIGHT - SIZE) head.setY(0);
	}
	
public void render(Graphics2D g2d) {
	int licznik = 0; 
	g2d.clearRect(0, 0, WIDTH, HEIGHT);
	g2d.setColor(Color.GREEN);
	//repaint();
	for(Entity e : snake) {
		if(licznik == 0 ) {
			e.headRender(g2d);
			++licznik;
		}
		else
			e.render(g2d);
	}
	
	g2d.setColor(Color.RED);
	for(Entity e : points) {
		e.render(g2d);
	}
	
	g2d.setColor(Color.BLUE);
	for(Entity e : superPoints) {
		e.renderSuperPoint(g2d);
	}
	
	if(gameover) {
		g2d.setColor(Color.WHITE);
		g2d.drawString("Game over!", 180, 100);	
	}
	
	g2d.setColor(Color.WHITE);
	g2d.drawString("Score: " + score + "  Level "+ level, 10, 10);
	
	if(dx==0 && dy==0 && source.StartMethod(start, false)) {
	g2d.drawString("Ready!", 180, 100);	
	}
	
	if(source.PauseMethod(pause, false)) {
		g2d.setColor(Color.WHITE);
		g2d.drawString("PAUSE", 180, 100);
	}
	
}



}
