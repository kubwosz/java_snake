package SnakeGame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.JSlider;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class MainFrame extends JFrame implements Runnable {

	private JPanel contentPane;
	
	private GamePanel panel;
	private JButton btnStart;
	private JButton btnPause;
	private JSlider slider ;
	
	private Thread thread, thread2;
	private boolean pause1;
	private int speed1;
	public Source source;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private boolean change(){
		boolean ret;
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(btnPause.getBackground() != Color.red)
					btnPause.setBackground(Color.red);
				else btnPause.setBackground(Color.GREEN);
			}		
		});
		if(btnPause.getBackground() != Color.red)
			ret = false;
		else ret = true;
		
		return ret;
	}
	private boolean changeStart(){
		boolean ret;
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnStart.setBackground(Color.GREEN);
			}		
		});
		if(btnStart.getBackground() == Color.GREEN) {
			ret = true;
		}
		else {
			ret = false;
			btnStart.setBackground(Color.gray);
		}
		btnStart.setBackground(Color.gray);
		
		return ret;
	}
	
	
	private int setSpeed(int speed) {
		int v = speed;
			v = slider.getValue();
		return v; 
	}
	
	public void run() {
		while(true) {
			
			source.PauseMethod(change(), true);
			source.StartMethod(changeStart(), true);
			speed1 = setSpeed(speed1);
			source.SpeedMethod(speed1, true);
		//	System.out.println(": " + source.speed );
				try{
					Thread.sleep(100);
				}catch(Exception e){
					e.printStackTrace();
				}
		}
		
	}
	
	
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 370);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][][][][]", "[][grow][][][][][][]"));
		
		source = new Source();
		panel = new GamePanel(source);
		
		contentPane.add(panel, "cell 0 0 5 6,grow");
		
		JLabel lblSpeed = new JLabel("Speed:");
		contentPane.add(lblSpeed, "cell 0 6");
		
		btnStart = new JButton("Start");
		contentPane.add(btnStart, "cell 3 6");
		
		btnPause = new JButton("Pause");
		btnPause.setBackground(Color.GREEN);
		
		slider = new JSlider();
		contentPane.add(slider, "cell 0 7");
		contentPane.add(btnPause, "cell 3 7");
		thread = new Thread(this);
		thread2 = new Thread(panel);
		thread.start();
		thread2.start();
	}

}
